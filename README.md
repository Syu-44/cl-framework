<h1 align="center">
  Fork of [M.Sc. Project](https://github.com/daspeks/masters-project) by Parthasarathi Das.
</h1>

Efficient C implementations of
* [Pailler](https://link.springer.com/chapter/10.1007/3-540-48910-X_16) Cryptosystem
* [Bresson, Catalano and Pointcheval](https://link.springer.com/chapter/10.1007/978-3-540-40061-5_3) Cryptosystem
* [Castagnos and Laguillaumie](https://eprint.iacr.org/2015/047.pdf) Cryptosystem

The master thesis that led to this implementation can be found [here](https://prism.ucalgary.ca/handle/1880/110888).

## Structure of the repository (For the CL-Framework)

- [inputs/CL/generator.cpp](inputs/CL/generator.cpp): This file contains the `Gen` function of the CL Framework. 
It Generates conductors $`f = (p_1...p_N)^t`$ and $`\Delta_{K} = -(p_1...p_N)q`$ for inputs (N, t, f bit-length, $`\Delta_{K}`$ bit-length). <br>
In the parent folder, the directory structure is important.
The directory name is the desired conductor bit length and contains subdirectories where the subdirectory names are desired $`\Delta_{K}`$ bit lengths.
Inside these directories you can find files labeled like so:
  - N_t_*f*.txt  : Conductor f = (p1...pN)^t
  - N_t_*fa*.txt : Conductor factorisation
  - N_t_*pr*.txt : Product of primes in the conductor
  - N_t_*dk*.txt : Fundamental discriminant $`\Delta_{K}`$
- [inputs/messages/generator.cpp](inputs/messages/generator.cpp): This file generates a thousand random messages for a given bit length.
The .txt files produced as output are directly accessible in the directory inputs/messages.
- [maxwell](maxwell): Contains a modified version the optarith library by Maxwell Sayles. 
You can find the required instructions to build it in the "Building the project" section of this readme.
- [src](src): At the root of this directory you can find the following files:
  - [maxheaders.hpp](src/maxheaders.hpp)     : Used to include all the .h files of liboptarith in a convenient way.
  - [measurements.hpp](src/measurements.hpp) : Used to record timings when a function of interest is called.
  - [QF.hpp](src/QF.hpp) : A class for quadratic ideals, it contains a lot of low level functions used by CL to compute with ideals.
  It is based on liboptarith and NTL.
  - [QFExponentiation.hpp](src/QFExponentiation.hpp) : A class for ideal exponentiation using ideal objects.
- [src/parameters/CL](src/parameters/CL): Contains the structures for the more "complex" arguments of the CL Framework, namely:
  - Ciphertexts $`(c_{1}, c_{2})`$
  - Plaintexts  $`m`$
  - PublicKey   $`pk`$
  - SecretKey   $`sk`$
- [src/systems/CL](src/systems/CL): In this folder you can find the core files of the CL Framework:
  - [Basic.hpp](src/systems/CL/Basic.hpp) : This is the implementation of the original CL Framework, as presented in [[CL15]](https://eprint.iacr.org/2015/047.pdf).
  It contains the functions `KeyGen`, `Encrypt`, `Decrypt`, `EvalScal` and `EvalSum`.
  - [Variant.hpp](src/systems/CL/Variant.hpp): This is Das's variant of the CL Cryptosystem.
  - [CL.hpp](src/systems/CL/CL.hpp): This file contains the subroutines used by both Basic and Variant, like the `Solve` function described in the original paper. 
- [tests/CL] : In this folder you can run test for the Basic and Variant implementation of the CL framework.
You can find more details in the "Running the tests for the CL Framework" section of this readme.

## Building the project
First of all make sure you have the following tools and libraries installed:
- [PARI](https://pari.math.u-bordeaux.fr/download.html)
- [GMP](https://gmplib.org/#DOWNLOAD)
- [NTL](https://libntl.org/doc/tour-unix.html)
- [SCons](https://scons.org/)

**Building antl**:
Go to the `antl` folder, edit [CONFIG](antl/CONFIG) to indicate where GMP and NTL are installed, change the CFLAGS if desired. 
Then execute `make appl` in the folder.

**Build your own version of [libmax.a](libmax.a)**:
```bash
> ln -s maxwell liboptarith
> cd maxwell
> scons
> ar cr libmax.a *.o
> mv libmax.a ../.
```

## Running the tests for the CL Framework
To run the test you first need to go to the [Basic](tests/CL/Basic) or [Variant](tests/CL/Variant) folder and run `make`, it will produce an executable named `t`.

You need to supply 3 paremeters to `t`:
- The bit length of the conductor $`f`$, which changes the message space size.
  - 16, 80, 256, 3072, 7680 or 15360
- The bit length of the fundamental discriminant $`\Delta_{K}`$, which determine the security level.
  - **1828**  =>  **128** bit security level
  - **3598**  =>  **192** bit security level
  - **5972**  =>  **256** bit security level
- Wheter the *short cipher* or the *long cipher* variant of the CL scheme is used. See [[DJS19, sec. 3.4]](https://prism.ucalgary.ca/handle/1880/110888)
  - **short**
  - **full**

For example if you executed `./t 16 1828 short`, this will create txt files with timings in the folder 16/1828.
**Caution**: The folders must be created before runnig the command, otherwise the txt files wont be written.

## Notes
- `maxwell` is a slightly modified version from `liboptarith` by [Maxwell Sayles](https://github.com/maxwellsayles).
- `antl` is due to [Michael J. Jacobson](https://pages.cpsc.ucalgary.ca/~jacobs/).
